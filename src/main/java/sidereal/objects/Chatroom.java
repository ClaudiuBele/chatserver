package sidereal.objects;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;

/** Represents a Chatroom. 
 * 
 * @author Claudiu
 *
 */
public class Chatroom {

	@Id
    private String id;

	/** When a user joins a room he is added to the list, and when he fails to receive a message from another user he
	 * is being removed.
	 */
	private List<Client> clients;
	
	/** List of messages over the last 5 minutes.
	 * 
	 */
	private List<String> messages;
	
	/** The name of the chat room. The room is created if not in the system or joined by the client on request
	 */
	private String name;
	
	public Chatroom()
	{
		clients = new ArrayList<Client>();
		messages = new ArrayList<String>();
		name = "Chatroom";
	}
	
	public Chatroom(String name)
	{
		this();
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Client> getClients() {
		return clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getMessages() {
		return messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
	
}
