package sidereal.objects;

import org.springframework.data.annotation.Id;

import sidereal.database.ClientRepository;

/** Client bean. Contains the nickname the user provided when logging into the app, as well
 * as the address of the address of the socket at which the client listens to input from the server.
 * <p>
 * The full address is compiled using HttpServletRequest parameters in the method that the client has,
 * and the port is hardcoded to 50357.
 * <p>
 * Will be stored in the database using {@link ClientRepository}.
 * 
 * @author Claudiu Bele
 *
 */
public class Client {

	@Id
    private String id;

	private String nickname;
	private String address;

    public Client(){}
    
    public Client(String nickname, String address) {
    	this.nickname = nickname;
    	this.address = address;
    }
    
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}


}