package sidereal;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;

import sidereal.utility.Encryption;
import sidereal.utility.ServerRequest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

@ComponentScan
@EnableAutoConfiguration
public class ChatApplication {

	public static enum MessageType{
		OK , WARNING, ERROR
	}
	
	private static StringBuilder builder;
	private final static ChatApplication instance = new ChatApplication();

	private long requestsPerMinute;
	private long dbCallsPerMinute;

	private long lastRequestsPerMinute;
	private long lastDbCallsPerMinute;

	private static String serverName;
	private static String serverFlag;

	// set the mapping of the connection servers to register to.
	public static final List<String> informationServers = new ArrayList<String>();

	/**
	 * Server creator. The server expects that arguments can be provided, stating initially the server name, flag to
	 * display and afterwards Information servers to contact. The format of the application is the following:
	 * <p>
	 * <code>"java -jar <jarname> <name=*name*> <flag=*flag*> <serverAddress#1> <servers=*server1&server2&server3&...> ..."</code>
	 * 
	 * 
	 * @param args The arguments passed from the command prompt or any other OS-dependent shell
	 *            <p>
	 *            the <b>server name</b> can be anything, but it should describe the host provider or the company that it is
	 *            maintaining/ supporting the server. If no value is given, a default one will be given with the following format
	 *            <code>Chat_Server_*timestamp*_*random value*
	 *            <p>
	 *            the <b>flag value</b> has to be name of the country for which to display the flag. The name of the country
	 *            has to start with a capital letter , e.g. Romania, France, United States of America. If no value is given, a default
	 *            one will be given ( "Unknown" ), which will display a question mark.
	 *            <p>
	 *            the <b>servers value</b> has to contain one or more servers, separated using the "&" character. The servers
	 *            have to also contain the port. If no value is given, a default one will be given for the localhost at port 50354
	 */
	public static void main(String[] args) {

		builder = new StringBuilder();
		
		// start server
		SpringApplication.run(ChatApplication.class, args);
	
		// get server name or default name if no name was passed in the arguments
		WeakReference<Random> rand = new WeakReference<Random>(new Random());
		ChatApplication.serverName = "Chat_Server_" + System.currentTimeMillis() + "_" + rand.get().nextInt(100000);
		
		ChatApplication.serverFlag = "Unknown";
		
		// parse arguments into server name, flag and server address.
		for (int i = 0; i < args.length; i++) {
			
			args[i] = args[i].replace("_", " ");
			
			// handle server name argument
			if (args[i].startsWith("name="))
			{
				ChatApplication.serverName = args[i].split("=")[1];
				appendToLog("Set server name to "+ChatApplication.serverName, MessageType.OK);
			}
			
			// handle flag argument
			if (args[i].startsWith("flag="))
			{
				ChatApplication.serverFlag = args[i].split("=")[1];
				appendToLog("Set server flag to "+ChatApplication.serverFlag, MessageType.OK);

			}

			// handle information servers
			if (args[i].startsWith("servers="))
				for (int j = 0; j < args[i].split("=")[1].split("&").length; j++) {
					informationServers.add(args[i].split("=")[1].split("&")[j]);
					appendToLog("Adding information server "+args[i].split("=")[1].split("&")[j], MessageType.OK);

				}

			
		}
		

		// add localhost by default, maybe there is a server there.
		informationServers.add("localhost:50354");
		// make a timer task that when ran, will contact all information servers with its own information
		// this is done in order to make sure we don't need to detect if an IS gets down
		TimerTask ISTask = new TimerTask() {

			public void run() {

				// tag as data that can be collected by GC
				SoftReference<String> toSend = new SoftReference<String>(ChatApplication.serverName+"{V}"+ChatApplication.serverFlag);
				
				// for each information server, we try to register
				for (int i = 0; i < informationServers.size(); i++) {
					// make final variable to use in annonymous inner class
					final String currInformationServer = informationServers.get(i);

					try {
						ChatApplication.get().incrementRequestsPerMinute();

						ServerRequest.POST("http://" + currInformationServer + "/chatServer", Encryption.encrypt(toSend.get().getBytes()));
						
						appendToLog("Successfully contacted information server "+currInformationServer, MessageType.OK);
							
												
					} catch (IOException e) {
						appendToLog("Unable to contact information server. Reason: " + e.getMessage(), MessageType.WARNING);
					}

				}
			}

		};
		final Timer infoTimer = new Timer();
		// schedule timer task to start right away, and afterwards run again every other minute.
		infoTimer.scheduleAtFixedRate(ISTask, 0, 60000);

		// make timer task in which every minute we reset the number of db calls and requests
		// , as well as save them in lastDbCallsPerMinute and lastRequestsPerMinute
		TimerTask ChSTask = new TimerTask() {

			@Override
			public void run() {
				instance.resetDbCallsPerMinute();
				instance.resetRequestsPerMinute();
			}
		};
		final Timer chatTimer = new Timer();
		// schedule timer to run every minute ( 60 * 1000 miliseconds => 60 * second => 1 minute )
		chatTimer.scheduleAtFixedRate(ChSTask, 60000, 60000);
		
		
		// task for making a log file
		TimerTask logTask = new TimerTask() {
			
			@Override
			public void run() {
				
				try {
					PrintWriter writer;
					
					// make directory if not existant
					File directory = new File("logs");
					if(!directory.exists()) directory.mkdirs();
					
					// make file if not existant
					File f = new File("logs/Log for "+System.currentTimeMillis()+".txt");
					if(!f.exists()) f.createNewFile();
					
					writer = new PrintWriter("logs/Log for "+System.currentTimeMillis()+".txt");
					writer.println(builder.toString());
					writer.flush();
					writer.close();
					
					new WeakReference<PrintWriter>(writer);
					new WeakReference<File>(f);
					
					builder = new StringBuilder();
					appendToLog("Successfully wrote log file", MessageType.OK);

					
				} catch (IOException e) {
					
					appendToLog("Unable to write log file. Reason: " + e.getMessage(), MessageType.WARNING);
				}
			}
		};
		final Timer logTimer = new Timer();
		// every 15 minutes
		logTimer.scheduleAtFixedRate(logTask, 1000 * 60 * 15 , 1000 * 60 * 15 );
		

	}

	public static ChatApplication get() {
		return instance;
	}

	public long getRequestsPerMinute() {
		return lastRequestsPerMinute;
	}

	public void incrementRequestsPerMinute() {
		this.requestsPerMinute++;
	}

	private void resetRequestsPerMinute() {
		this.lastRequestsPerMinute = requestsPerMinute;
		requestsPerMinute = 0;
	}

	public long getDbCallsPerMinute() {
		return lastDbCallsPerMinute;
	}

	public void incrementDbCallsPerMinute() {
		this.dbCallsPerMinute++;
	}

	private void resetDbCallsPerMinute() {
		this.lastDbCallsPerMinute = dbCallsPerMinute;
		dbCallsPerMinute = 0;
	}
	
	public static void appendToLog(String line, MessageType type)
	{
		line = type.toString()+" : "+line;
		System.out.println(line);
		builder.append(line);
		builder.append("\r\n");
		
	}

}
