package sidereal.database;


import org.springframework.data.mongodb.repository.MongoRepository;

import sidereal.objects.Client;

public interface ClientRepository extends MongoRepository<Client, String>
{
	// implemented when the application runs, so I don't have to do stuff
	// those methods are only declared so the functionality is to be added by the system
	 public Client findByNickname(String nickname);
	 public Client findByAddress(String address);
}
