package sidereal.database;


import org.springframework.data.mongodb.repository.MongoRepository;

import sidereal.objects.Chatroom;

public interface ChatroomRepository extends MongoRepository<Chatroom, String>
{
	// implemented when the application runs, so I don't have to do stuff
	// those methods are only declared so the functionality is to be added by the system
	 public Chatroom findByName(String name);
	
}
