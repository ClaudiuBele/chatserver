package sidereal.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sidereal.ChatApplication;
import sidereal.ChatApplication.MessageType;
import sidereal.utility.Encryption;

@RestController
public class APIController {

	
	@RequestMapping(value = "/usage", method = RequestMethod.GET)
	public final byte[] getServerUsage() {
		ChatApplication.get().incrementRequestsPerMinute();
		
		String text = ChatApplication.get().getDbCallsPerMinute()+"{V}"+ChatApplication.get().getRequestsPerMinute();
		ChatApplication.appendToLog("Connection server requested usage from server, usage is "+text, MessageType.OK);
		return Encryption.encrypt(text.getBytes());
	}
	
}
