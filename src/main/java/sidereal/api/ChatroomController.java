package sidereal.api;

import java.io.UnsupportedEncodingException;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sidereal.ChatApplication;
import sidereal.ChatApplication.MessageType;
import sidereal.database.ChatroomRepository;
import sidereal.database.ClientRepository;
import sidereal.objects.Chatroom;
import sidereal.objects.Client;
import sidereal.utility.Encryption;
import sidereal.utility.ServerMessage;
import sidereal.utility.ServerRequest;

@RestController
public class ChatroomController {

	@Autowired
	private ChatroomRepository roomRepo;

	@Autowired
	private ClientRepository clientRepo;

	@RequestMapping(value = "/joinchatroom", method = RequestMethod.POST)
	public final byte[] joinChatroom(@RequestBody byte[] reqData, HttpServletRequest request)
			throws UnsupportedEncodingException {
		

		ChatApplication.get().incrementRequestsPerMinute();

		// return if data is empty
		if(reqData == null || reqData.length == 0 )
		{
			WeakReference<HttpServletRequest> gcReference = new WeakReference<HttpServletRequest>(request);
			return Encryption.encrypt(new String(ServerMessage.DATA_EMPTY+"").getBytes());
		}
		
		ChatApplication.get().incrementDbCallsPerMinute();
		
		// get chat room name
		String chatroomName = new String(Encryption.decrypt(reqData), "UTF-8");
		chatroomName = chatroomName.toLowerCase();
		
		Client client = null;
		try {

			client = clientRepo.findByAddress(request.getRemoteAddr() + ":" + 50357);
		} catch (Exception e) {
			ChatApplication.appendToLog("Unable to contact MongoDB server in order to add client to chatroom",
					MessageType.ERROR);
			return Encryption.encrypt(new String(ServerMessage.CLIENT_NOT_AUTHENTICATED + "").getBytes());
		}

		if (client == null) {
			ChatApplication.appendToLog("Client at address " + request.getRemoteAddr() + ":" + 50357
					+ " tried to connect to chatroom, but has not authenticated to it", MessageType.OK);
			return Encryption.encrypt(Integer.toString(ServerMessage.CLIENT_NOT_AUTHENTICATED).getBytes());

		}

		// try to find the chatroom that the client entered
		ChatApplication.get().incrementDbCallsPerMinute();
		Chatroom myChatroom = roomRepo.findByName(chatroomName);

		// chatroom exists
		if (myChatroom != null) {

			// chatroom is full
			if (myChatroom.getClients().size() >= 50) {
				ChatApplication.appendToLog("Client at address " + request.getRemoteAddr() + ":" + 50357
						+ " tried to connect to chatroom, but room \"" + chatroomName + "\" is full", MessageType.OK);
				return Encryption.encrypt(Integer.toString(ServerMessage.CHATROOM_FULL).getBytes());
			}

			// remove old messages
			verifyMessageTimes(myChatroom);

			// do not add if already in there
			for (int i = 0; i < myChatroom.getClients().size(); i++) {
				if (myChatroom.getClients().get(i).getAddress().equals(request.getRemoteAddr() + ":" + 50357)) {
					ChatApplication.appendToLog("Client at address " + request.getRemoteAddr() + ":" + 50357
							+ " tried to connect to chatroom, but he is already in \"" + chatroomName + "\" is full",
							MessageType.OK);
					myChatroom.getClients().get(i).setNickname(client.getNickname());
					roomRepo.save(myChatroom);
					return Encryption.encrypt(Integer.toString(ServerMessage.CHATROOM_ALREADY_JOINED).getBytes());
				}
			}

			// try to send message
			submitData(myChatroom,
					addMessage(myChatroom, client, ServerMessage.toString(ServerMessage.CHATROOM_ENTERED)), client);

			myChatroom.getClients().add(client);

			// save new data to repository
			roomRepo.save(myChatroom);

			// data to send to client joining the server.
			StringBuilder builder = new StringBuilder();
			builder.append(myChatroom.getName());
			builder.append("{M}");
			for (int i = 0; i < myChatroom.getMessages().size(); i++) {
				builder.append(myChatroom.getMessages().get(i));
				if (i != myChatroom.getMessages().size() - 1)
					builder.append("{O}");
			}

			ChatApplication.appendToLog(client.getNickname() + " using address " + client.getAddress()
					+ " has successfully connected to chatroom " + chatroomName, MessageType.OK);
			new SoftReference<StringBuilder>(builder);

			return Encryption.encrypt(builder.toString().getBytes());
		}
		// chatroom doesn't exist
		else {
			myChatroom = new Chatroom(chatroomName);
			addMessage(myChatroom, client, ServerMessage.toString(ServerMessage.CHATROOM_ENTERED));
			myChatroom.getClients().add(client);
			// save new data to repository
			roomRepo.save(myChatroom);

			ChatApplication.appendToLog(client.getNickname() + " using address " + client.getAddress()
					+ " has successfully created chatroom " + chatroomName, MessageType.OK);
			return Encryption.encrypt(Integer.toString(ServerMessage.CHATROOM_CREATED).getBytes());
		}

	}

	/**
	 * Writing data to a chatroom
	 * <p>
	 * 
	 * @param reqData
	 *            encrypted data from client, contains the chatroom name and the data, delimited by {V}. The data is
	 *            encrypted using AES
	 * 
	 * @param request
	 *            used for retrieving remote address
	 * @return Message based on the status of the request to send message.
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping(value = "/chatroom", method = RequestMethod.POST)
	public final byte[] writeToChatroom(@RequestBody byte[] reqData, HttpServletRequest request)
			throws UnsupportedEncodingException {
		
		
		ChatApplication.get().incrementRequestsPerMinute();

		// return if data is empty
		if(reqData == null || reqData.length == 0)
		{
			WeakReference<HttpServletRequest> gcReference = new WeakReference<HttpServletRequest>(request);
			return Encryption.encrypt(new String(ServerMessage.DATA_EMPTY+"").getBytes());
		}
		
		String[] decryptedData = new String(Encryption.decrypt(reqData), "UTF-8").split("\\{V\\}");
		// get chat room name
		String chatroomName = decryptedData[0];
		chatroomName = chatroomName.toLowerCase();
		String data = decryptedData[1];
		
		// get the client from which the request was made
		ChatApplication.get().incrementDbCallsPerMinute();

		Client client = null;
		try {
			client = clientRepo.findByAddress(request.getRemoteAddr() + ":" + 50357);
		} catch (Exception e) {
			ChatApplication.appendToLog("Unable to contact MongoDB server in order for client to write to chatroom",
					MessageType.ERROR);
			return Encryption.encrypt(new String(ServerMessage.CLIENT_NOT_AUTHENTICATED + "").getBytes());
		}

		if (client == null)
			return Encryption.encrypt(Integer.toString(ServerMessage.CLIENT_NOT_AUTHENTICATED).getBytes());

		// try to find the chatroom that the client entered
		ChatApplication.get().incrementDbCallsPerMinute();
		Chatroom myChatroom = roomRepo.findByName(chatroomName);

		if (myChatroom != null) {
			// remove old messages
			verifyMessageTimes(myChatroom);

			// try to send message
			submitData(myChatroom, addMessage(myChatroom, client, data), client);

			// save new data to repository
			roomRepo.save(myChatroom);
			ChatApplication.get().incrementDbCallsPerMinute();

			return Encryption.encrypt(Integer.toString(ServerMessage.CHATROOM_MESSAGE_SENT).getBytes());
		} else {
			myChatroom = new Chatroom(chatroomName);
			myChatroom.getClients().add(client);
			addMessage(myChatroom, client, ServerMessage.toString(ServerMessage.CHATROOM_ENTERED));

			roomRepo.save(myChatroom);

			return Encryption.encrypt(Integer.toString(ServerMessage.CHATROOM_CREATED).getBytes());
		}
	}

	@RequestMapping(value = "/exitChatroom", method = RequestMethod.POST)
	public final byte[] exitChatroom(@RequestBody byte[] reqData, HttpServletRequest request)
			throws UnsupportedEncodingException {
		
		
		ChatApplication.get().incrementRequestsPerMinute();

		// return if data is empty
		if(reqData == null || reqData.length == 0)
		{
			WeakReference<HttpServletRequest> gcReference = new WeakReference<HttpServletRequest>(request);
			return Encryption.encrypt(new String(ServerMessage.DATA_EMPTY+"").getBytes());
		}
		
		// get chat room name
		String chatroomName = new String(Encryption.decrypt(reqData), "UTF-8");
		chatroomName = chatroomName.toLowerCase();

		// get the client from which the request was made
		Client client = null;
		try {
			ChatApplication.get().incrementDbCallsPerMinute();
			client = clientRepo.findByAddress(request.getRemoteAddr() + ":" + 50357);
		} catch (Exception e) {
			ChatApplication.appendToLog("Unable to contact MongoDB server in order for client to leave chatroom",
					MessageType.ERROR);
			return Encryption.encrypt(new String(ServerMessage.CLIENT_NOT_AUTHENTICATED + "").getBytes());
		}

		if (client == null)
			return Encryption.encrypt(Integer.toString(ServerMessage.CLIENT_NOT_AUTHENTICATED).getBytes());

		// try to find the chatroom that the client entered
		ChatApplication.get().incrementDbCallsPerMinute();
		Chatroom myChatroom = roomRepo.findByName(chatroomName);

		if (myChatroom != null) {
			ChatApplication.appendToLog(client.getNickname() + " using address " + client.getAddress()
					+ " has successfully exitted from chatroom " + chatroomName, MessageType.OK);

			// remove old messages
			verifyMessageTimes(myChatroom);

			// try to send message
			submitData(myChatroom, addMessage(myChatroom, client, ServerMessage.toString(ServerMessage.CHATROOM_LEFT)),
					client);

			removeClient(myChatroom, client);
			ChatApplication.get().incrementDbCallsPerMinute();

			if (myChatroom.getClients().size() == 0)
				// delete chatroom if noone is in
				roomRepo.delete(myChatroom);
			else
				// save new data to repository
				roomRepo.save(myChatroom);

			ChatApplication.get().incrementDbCallsPerMinute();
			return Encryption.encrypt(Integer.toString(ServerMessage.CHATROOM_LEFT).getBytes());

		} else {
			return null;
		}
	}

	private void submitData(Chatroom chatroom, String data, Client source) {

		ChatApplication.appendToLog("Sending message \"" + data + "\" from client " + source + " in chatroom "
				+ chatroom, MessageType.OK);

		ArrayList<Client> clientsToRemove = new ArrayList<Client>();

		data = chatroom.getName() + "{M}" + data;
		// tries to send data to all clients in the chatroom
		for (int i = 0; i < chatroom.getClients().size(); i++) {
			if (source.getAddress().equals(chatroom.getClients().get(i).getAddress()))
				continue;
			// try to send data to clients, if they fail to receive them remove them

			// try to send message to all users
			try {

				ServerRequest.sendToTCPServer(chatroom.getClients().get(i).getAddress(),
						Encryption.encrypt(data.getBytes()));
				ChatApplication.get().incrementRequestsPerMinute();
			} catch (Exception e) {
				// failed to send data to client, adding user to list of objects to remove
				clientsToRemove.add(chatroom.getClients().get(i));
			}

		}

		// builder in which we will put all of the messages
		StringBuilder disconnectedClientsBuilder = new StringBuilder();

		// go over the clients that failed to get the data, assume they are offline.
		for (int i = 0; i < clientsToRemove.size(); i++) {
			// trying to delete the client from the repo
			try {

				ChatApplication.appendToLog(
						clientsToRemove.get(i).getNickname() + " using address " + clientsToRemove.get(i)
								+ " has been from chatroom " + chatroom.getName()
								+ " due to not being able to send data to", MessageType.OK);
				clientRepo.delete(clientsToRemove.get(i));
				ChatApplication.get().incrementDbCallsPerMinute();

				// if we actually find the client in the chatroom, we remove it and add
				// a text to display
				if (removeClient(chatroom, clientsToRemove.get(i))) {
					disconnectedClientsBuilder.append(addMessage(chatroom, clientsToRemove.get(i),
							ServerMessage.toString(ServerMessage.CHATROOM_LEFT)));
					disconnectedClientsBuilder.append("{O}");
				}
			} catch (Exception e) {
			}
		}

		// submit the data about disconnected users
		if (disconnectedClientsBuilder.toString().length() != 0)
			submitData(chatroom, disconnectedClientsBuilder.toString(), source);

		new WeakReference<ArrayList<Client>>(clientsToRemove);
	}

	private String addMessage(Chatroom chatroom, Client client, String message) {
		String chatroomMessage = System.currentTimeMillis() + "{V}<" + client.getNickname() + "> : " + message;
		chatroom.getMessages().add(chatroomMessage);
		return chatroomMessage;
	}

	private void verifyMessageTimes(Chatroom chatroom) {
		while (chatroom.getMessages().size() > 0) {
			long messageTime = Long.parseLong(chatroom.getMessages().get(0).split("\\{V\\}")[0]);
			// 5 minutes have passed since adding
			if (System.currentTimeMillis() - messageTime > 1000 * 60 * 5)
				chatroom.getMessages().remove(0);
			else
				return;
		}
	}

	private boolean removeClient(Chatroom from, Client client) {
		for (int i = 0; i < from.getClients().size(); i++) {
			if (from.getClients().get(i).getAddress().equals(client.getAddress())) {
				from.getClients().remove(i);
				return true;
			}
		}
		return false;
	}

}
