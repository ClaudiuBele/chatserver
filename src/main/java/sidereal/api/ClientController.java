package sidereal.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sidereal.ChatApplication;
import sidereal.ChatApplication.MessageType;
import sidereal.database.ClientRepository;
import sidereal.objects.Client;
import sidereal.utility.Encryption;
import sidereal.utility.ServerMessage;
import sidereal.utility.ServerRequest;

@RestController
public class ClientController {

	@Autowired
	private ClientRepository repository;
	
	/** Method called when logging in to a chat room from the client app. The same flow applies to all
	 * platforms.
	 * 
	 * @param reqData Data sent with the POST request
	 * @param request Auto-generated object from which to extract address.
	 * @return a value, true/false, for whether or not connection has been made.
	 * @throws IOException 
	 */
	@RequestMapping(value = "/client", method = RequestMethod.POST)
	public final byte[] getChatServers(@RequestBody byte[] reqData, HttpServletRequest request) throws IOException {
		
		
		
		// increase number of requests and get client information
		ChatApplication.get().incrementRequestsPerMinute();
		
		// return if data is empty
		if(reqData == null || reqData.length == 0 || reqData.length % 16 != 0)
		{
			WeakReference<HttpServletRequest> gcReference = new WeakReference<HttpServletRequest>(request);
			return Encryption.encrypt(new String(ServerMessage.DATA_EMPTY+"").getBytes());
		}

		
		String nickname = new String(Encryption.decrypt(reqData),"UTF-8");
		String address = request.getRemoteAddr()+":"+50357;
		
		Client potentialDuplicate = null;
		try{
			
			potentialDuplicate =  repository.findByAddress(request.getRemoteAddr()+":"+50357);
		}
		catch(Exception e)
		{
			ChatApplication.appendToLog("Unable to contact MongoDB server in order to add client",MessageType.ERROR);
			return Encryption.encrypt(new String(ServerMessage.CLIENT_NOT_AUTHENTICATED+"").getBytes());
		}	
		
		
		if(potentialDuplicate != null)
		{
			if(potentialDuplicate.getNickname().equals(nickname)) 
			{
				ChatApplication.appendToLog("Client has already been found in the system, not adding", MessageType.WARNING);
				return Encryption.encrypt(new String(ServerMessage.CLIENT_ENTERED+"").getBytes());
			}

			potentialDuplicate.setNickname(nickname);
			repository.save(potentialDuplicate);
		}
		else
		{
			repository.save(new Client(nickname,address));
		}
		ChatApplication.get().incrementDbCallsPerMinute();
		ChatApplication.appendToLog("Successfully added client "+nickname+" to server",MessageType.OK);

		return Encryption.encrypt("true".getBytes());
	}
}
